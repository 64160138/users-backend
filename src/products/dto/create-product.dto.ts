import { IsNotEmpty, MinLength } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  id: number;

  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  @MinLength(5)
  price: number;
}
